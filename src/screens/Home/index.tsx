import {useNavigation} from '@react-navigation/native';
import React, {useEffect} from 'react';
import {Dimensions, Text, TouchableOpacity, View} from 'react-native';
import {StackNavigation} from 'src/navigation';
import useStore from 'src/stores/useStore';

const Home = () => {
  const navigation: StackNavigation = useNavigation();

  const {bears, increase, decrease, fetchData, data, loading, error} =
    useStore();

  return (
    <View
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        width: Dimensions.get('screen').width,
        height: Dimensions.get('screen').height,
        gap: 8,
      }}>
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          gap: 10,
        }}>
        <TouchableOpacity
          style={{
            backgroundColor: 'white',
            paddingHorizontal: 12,
            paddingVertical: 8,
            shadowOpacity: 0.1,
          }}
          onPress={() => decrease(1)}>
          <Text>{'-'}</Text>
        </TouchableOpacity>
        <Text>{bears}</Text>
        <TouchableOpacity
          style={{
            backgroundColor: 'white',
            paddingHorizontal: 12,
            paddingVertical: 8,
            shadowOpacity: 0.1,
          }}
          onPress={() => increase(1)}>
          <Text>{'+'}</Text>
        </TouchableOpacity>
      </View>
      <TouchableOpacity onPress={() => navigation.navigate('SecondHome')}>
        <Text>HomeScreen</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => fetchData(bears)}>
        <Text>Fetch Data</Text>
      </TouchableOpacity>
      <Text>{loading ? 'Loading...' : data?.name}</Text>
    </View>
  );
};

export default Home;
