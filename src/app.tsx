import {NavigationContainer} from '@react-navigation/native';
import * as React from 'react';
import RootNavigation from 'src/navigation';

function App() {
  return <RootNavigation />;
}

export default App;
