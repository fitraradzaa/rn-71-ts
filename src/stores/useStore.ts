import AsyncStorage from '@react-native-async-storage/async-storage';
import {create} from 'zustand';
import {persist, createJSONStorage} from 'zustand/middleware';

interface IData {
  name: string;
  address: string;
  username: string;
}

interface IBearState {
  bears: number;
  loading: boolean;
  error: boolean;
  data?: IData | null;
  increase: (by: number) => void;
  decrease: (by: number) => void;
  fetchData: (id: number) => void;
}

const initialState = {
  bears: 0,
  loading: false,
  error: false,
  data: null,
};

const useStore = create<IBearState>()(
  persist(
    set => ({
      ...initialState,
      increase: by => set(state => ({bears: state.bears + by})),
      decrease: by => set(state => ({bears: state.bears - by})),
      fetchData: async id => {
        try {
          set({loading: true});
          const getData = await fetch(
            `https://jsonplaceholder.typicode.com/users/${id}`,
          );
          const response: IData = await getData.json();
          set({data: response});
        } catch (err) {
          console.log('err', err);
          set({error: true});
        } finally {
          set({loading: false});
        }
      },
    }),
    {
      name: 'food-storage',
      storage: createJSONStorage(() => AsyncStorage),
    },
  ),
);

export default useStore;
