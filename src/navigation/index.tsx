import React from 'react';

import {NavigationContainer, NavigationProp} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Home from 'src/screens/Home';
import SecondHome from 'src/screens/SecondHome';

export type ScreenNames = ['Home', 'SecondHome'];
export type RootStackParamList = Record<ScreenNames[number], undefined>;
export type StackNavigation = NavigationProp<RootStackParamList>;

const Stack = createNativeStackNavigator();

const RootNavigation = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="SecondHome" component={SecondHome} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RootNavigation;
